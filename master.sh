#!/bin/bash

# v0.1.1

# This is a helper script which currently fulfills 3 functions:
# 1. getOS: get the Operating System and version we are running on
# 2. MSG: a function that gives nicely formatted screen output during runtime
# 3. INSTALL_PKG: a function that non-interactively installs a debian package

getOS(){
    if [ -f /etc/os-release ]; then
        # freedesktop.org and systemd
        . /etc/os-release
        OS=$ID
        if [[ $ID_LIKE ]]; then
            OS=$ID_LIKE
        fi
        export OS_VERSION_NUM=$VERSION_ID
    elif type lsb_release >/dev/null 2>&1; then
        # linuxbase.org
        OS=$(lsb_release -si)
        export OS_VERSION_NUM=$(lsb_release -sr)
    elif [ -f /etc/lsb-release ]; then
        # For some versions of Debian/Ubuntu without lsb_release command
        . /etc/lsb-release
        OS=$DISTRIB_ID
        export OS_VERSION_NUM=$DISTRIB_RELEASE
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS=Debian
        export OS_VERSION_NUM=$(cat /etc/debian_version)
    elif [ -f /etc/SuSe-release ]; then
        # Older SuSE/etc.
        OS=$(cat /etc/SuSe-release)
    elif [ -f /etc/redhat-release ]; then
        # Older Red Hat, CentOS, etc.
        OS=$(cat /etc/redhat-release)
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS=$(uname -s)
        export OS_VERSION=$(uname -r)
    fi

    # Check if $OS_VERSION_NUM is string or number
    case $OS_VERSION_NUM in
        ''|*[!0-9]*)
            # # Convert OS_VERSION_NUM from string to float
            OS_VERSION_NUM=$(printf -v int '%d\n' "$OS_VERSION_NUM" 2>/dev/null)
        ;;
        *)
            # Do nothing
            :
        ;;
    esac


    # Convert the version name to lowercase
    OS_INTERNAL=$(echo "$OS" | tr '[:upper:]' '[:lower:]')

    # Check for known names and reassign $OS to a unified one-word distro name
    if [[ $OS_INTERNAL == *"debian"* ]]; then
        export OS="Debian"
        export OS_FAM="Debian"
    elif [[ $OS == *"Ubuntu"* ]]; then
        export OS="Ubuntu"
        export OS_FAM="Debian"
    elif [[ $OS == *"red hat"* ]]; then
        export OS="RedHat"
        export OS_FAM="RedHat"
    elif [[ $OS == *"centos"* ]]; then
        export OS="CentOS"
        export OS_FAM="RedHat"
    elif [[ $OS == *"suse"* ]]; then
        export OS="Suse"
        export OS_FAM="Suse"
    elif [[ $OS == *"bsd"* ]]; then
        export OS="bsd"
        export OS_FAM="bsd"
    else
        export OS=$OS
        export OS_FAM="Unknown"
    fi

    #printf "OS: $OS Version: $OS_VERSION_NUM\n\n"

}

MSG(){
    # Colourful output ftw!
    RED='\033[0;31m'
    YELLOW='\33[1;33m'
    GREEN='\033[0;32m'
    NC='\033[0m'

    if [ "$1" == "ERROR" ]
    then
        printf "${RED}ERROR: ${NC} $2 \n\n" 1>&2

        # Stop script execution; but since we are in a subshell (function!) here,
        # we need to use our trap...
        kill -s TERM $TOP_PID
    elif [ "$1" == "INFO" ]
    then
        printf "${YELLOW}INFO: ${NC} $2 \n\n" 1>&2
    elif [ "$1" == "SUCCESS" ]
    then
        printf "${GREEN}SUCCESS: ${NC} $2 \n\n" 1>&2
    else
        printf "MSG: ${NC} $2 \n\n" 1>&2
    fi
}

INSTALL_PKG(){
    #printf "Installing needed packages: $@"
    MSG INFO "Installing needed packages: $@"
    DEBIAN_FRONTEND=noninteractive apt-get  -qq -y install $@  >/dev/null 2>&1 || MSG ERROR "installing $@"
}
